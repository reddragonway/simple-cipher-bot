#!/usr/bin/env bash
touch /home/rdw/cipherbot_users.log || true
docker login -u $REGISTRY_USER -p $REGISTRY_PASSWORD $REGISTRY_URL
docker pull registry.gitlab.com/reddragonway/cipher-bot/cipher-bot:latest
docker stop cipher-bot || true
docker rm cipher-bot || true
docker run -d --name cipher-bot \
    -v /home/rdw/cipherbot_users.log:/app/cipherbot_users.log \
    --restart always \
    --pull always \
    registry.gitlab.com/reddragonway/cipher-bot/cipher-bot:latest
docker image prune -f
